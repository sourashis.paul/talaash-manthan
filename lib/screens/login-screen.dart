import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';
import 'package:CRS/service/user_auth.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:CRS/screens/dashboard_screen.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:sms_autofill/sms_autofill.dart';
import '../service/rounded-button.dart';
import 'package:CRS/service/variables.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  dynamic decodedData;
  dynamic profileDecodedData;
  String email;
  String password;
  bool showspinner = false;
  GlobalKey<FormState> _form = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: showspinner,
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                Flexible(
                  child: Center(
                    child: Container(
                      height: 100,
                      width: 150,
                      child: Image(
                        image: AssetImage('images/police.png'),
                      ),
                    ),
                  ),
                ),
                Form(
                  key: _form,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Enter Email',
                              labelText: 'Email'),
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            email = value;
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'This field is required';
                            }
                            if (!RegExp(
                                    r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                .hasMatch(value)) {
                              return "Please enter a valid email address";
                            } else {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            }
                            // the email is valid
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: TextFormField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'Enter Password',
                                labelText: 'Password'),
                            keyboardType: TextInputType.visiblePassword,
                            onChanged: (value) {
                              password = value;
                            },
                            obscureText: true,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'This field is required';
                              } else {
                                if (!RegExp(
                                        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                                    .hasMatch(value)) {
                                  _showMyDialog('Invalid Password',
                                      'Your password must:\n\nContain at least:- \n>>8 characters.\n>>1 Digit.\n>>1 Uppercase Letter.\n>>1 Lowercase Letter.\n>>1 Special Character.');
                                  return 'Enter valid password';
                                } else {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());

                                  return null;
                                }
                              }
                            }),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 25.0),
                        child: GestureDetector(
                          onTap: () =>
                              Navigator.pushNamed(context, 'ForgotPassword'),
                          child: Text(
                            'Forgot Password',
                            style: TextStyle(
                              fontSize: 22,
                              color: kBlue,
                              height: 1.5,
                              fontFamily: 'PTSans',
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Center(
                        child: RoundedButton(
                          ontap: () async {
                            if (_form.currentState.validate()) {
                              setState(() {
                                showspinner = true;
                              });
                              http.Response response =
                                  await citizenlogin(email, password);
                              print(response.statusCode);
                              String data = response.body;
                              decodedData = jsonDecode(data);
                              if (response.statusCode == 200 ||
                                  response.statusCode == 201) {
                                Variables.token = decodedData['token'];
                                http.Response response = await getprofileinfo(
                                    Variables.token, email);
                                print(response.statusCode);
                                print(response.body.toString());
                                String profileData = response.body;
                                profileDecodedData = jsonDecode(profileData);
                                Variables.name = profileDecodedData['name'];
                                Variables.email = profileDecodedData['email'];
                                Variables.id = profileDecodedData['_id'];
                                Variables.phone = profileDecodedData['phone'];
                                Variables.address =
                                    profileDecodedData['address'];
                                Variables.pinCode =
                                    profileDecodedData['pincode'];
                                Variables.policeStation =
                                    profileDecodedData['policeStation'];
                                Variables.state = profileDecodedData['state'];
                                Variables.district =
                                    profileDecodedData['district'];
                                Variables.division =
                                    profileDecodedData['division'];
                                Variables.image = profileDecodedData['image'];
                                setState(() {
                                  showspinner = false;
                                });
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        DashBoard(),
                                  ),
                                );
                              } else {
                                _showMyDialog('Error: ${response.statusCode}',
                                    decodedData['message']);
                              }
                              setState(() {
                                showspinner = false;
                              });
                            }
                          },
                          buttonName: 'Login',
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Do not have an account?',
                      style: kBodyText,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, 'CreateNewAccount');
                        final appSignature = SmsAutoFill().getAppSignature;
                        print(appSignature);
                      },
                      child: Text(
                        ' Register',
                        style: kBodyText.copyWith(
                            color: kBlue, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
