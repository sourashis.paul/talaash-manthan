import 'package:CRS/service/user_auth.dart';
import 'package:date_format/date_format.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ComplaintDetailsScreen extends StatefulWidget {
  ComplaintDetailsScreen({@required this.cId});
  final String cId;
  @override
  _ComplaintDetailsScreenState createState() =>
      _ComplaintDetailsScreenState(cId: cId);
}

class _ComplaintDetailsScreenState extends State<ComplaintDetailsScreen> {
  _ComplaintDetailsScreenState({@required this.cId});
  final String cId;
  bool showspinner = false;
  String dateTime;
  String date;
  String type;
  String title;
  String time;
  String desc;
  String cStatus;
  String state;
  String district;
  String division;
  String policeStation;
  String newCID;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: kBlue,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: kWhite,
          ),
        ),
        title: Text(
          'Complaint Details',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: getFullComplaintDetails(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ComplaintDeatilsAfterLoad(
                  comId: newCID,
                  date: date,
                  type: type,
                  title: title,
                  policeStation: policeStation,
                  division: division,
                  district: district,
                  state: state,
                  time: time,
                  desc: desc,
                  cStatus: cStatus);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  //--------------------------------------------------- Api Call -----

  Future getFullComplaintDetails() async {
    http.Response response = await getComplaintDetails(cId);
    print(response.statusCode);
    dynamic data = response.body;
    dynamic decodedData = jsonDecode(data);
    if (response.statusCode == 200 || response.statusCode == 201) {
      dateTime = decodedData['timeOfCrime'];
      type = decodedData['type'];
      time = getTime(dateTime);
      date = getDate(dateTime);
      title = decodedData['title'];
      desc = decodedData['description'];
      cStatus = decodedData['status'];
      state = decodedData['state'];
      district = decodedData['district'];
      division = decodedData['division'];
      policeStation = decodedData['policeStation'];
      print('Done');
      newCID = cId.substring(cId.length - 9);
    } else {
      _showMyDialog('${response.statusCode}', 'Try Again!');
    }
  }

  String getDate(String time) {
    var date = DateTime.parse(time);
    String timezone = DateFormat.yMMMd().format(date);
    return timezone;
  }

  String getTime(String time) {
    var date = DateTime.parse(time);
    String timemode = formatDate(date, [HH, ':', nn, ':', ss]);
    return timemode;
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class ComplaintDeatilsAfterLoad extends StatelessWidget {
  const ComplaintDeatilsAfterLoad({
    Key key,
    @required this.comId,
    @required this.date,
    @required this.type,
    @required this.title,
    @required this.policeStation,
    @required this.division,
    @required this.district,
    @required this.state,
    @required this.time,
    @required this.desc,
    @required this.cStatus,
  }) : super(key: key);
  final String comId;
  final String date;
  final String type;
  final String title;
  final String policeStation;
  final String division;
  final String district;
  final String state;
  final String time;
  final String desc;
  final String cStatus;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Container(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '#' + comId.toUpperCase(),
                      style: complaintDetailsScreenIdStyle,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        right: 10.0,
                        left: 10.0,
                        top: 5.0,
                        bottom: 5.0,
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFF50934B),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Text(
                        date,
                        style: complaintDetailsScreenDateStyle,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  padding: EdgeInsets.only(right: 10.0, left: 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xFFFFC7C7),
                    border: Border.all(width: 1.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Text(
                    type,
                    style: complaintDetailsScreenTypeStyle,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Text(
                  title,
                  style: complaintDetailsScreenTitleStyle,
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 10.0, right: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.add_location,
                          color: Colors.red,
                          size: 25.0,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          '$policeStation, $division,\n$district, $state',
                          style: complaintDetailsScreenLocationStyle,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.history_toggle_off_sharp,
                          size: 25.0,
                        ),
                        SizedBox(
                          width: 5.0,
                        ),
                        Text(
                          time,
                          // '10 : 00 AM',
                          style: complaintDetailsScreenTimeStyle,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                // height: 250.0,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 10.0, right: 10.0),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  // color: Colors.grey.shade400,
                  border: Border.all(
                    color: Colors.grey.shade400,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: 7.0,
                        right: 7.0,
                        top: 2,
                        bottom: 2,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.grey.shade300,
                        border: Border.all(width: 1.0),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Text(
                        'Description',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      desc,
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.attachment_rounded,
                            size: 27.0,
                            color: Colors.red.shade700,
                          ),
                        ),
                        Text(
                          'Attached File...',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 10.0, right: 10.0),
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.shade400,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: 7.0,
                        right: 7.0,
                        top: 2,
                        bottom: 2,
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFFB0FFBA),
                        border: Border.all(width: 1.0),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text(
                        'Status',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      cStatus,
                      style: complaintDetailsScreenStatusStyle,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              FlatButton(
                onPressed: () {},
                child: Container(
                  margin: EdgeInsets.only(right: 10.0, left: 10.0),
                  height: 40.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Color(0xFFAA2B2B),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Center(
                    child: Text(
                      'Withdraw',
                      style: complaintDetailsScreenButtonStyle,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
