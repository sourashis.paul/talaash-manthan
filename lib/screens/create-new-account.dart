import 'dart:ui';
import 'dart:io';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:CRS/service/round_icon_button.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http_parser/http_parser.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:CRS/service/user_auth.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:dropdown_search/dropdown_search.dart';

List<GlobalKey<FormState>> formKeys = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

class CreateNewAccount extends StatefulWidget {
  @override
  _CreateNewAccountState createState() => _CreateNewAccountState();
}

class _CreateNewAccountState extends State<CreateNewAccount> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;
  TextEditingController dateCtl = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final password = TextEditingController();
  final confirmPassword = TextEditingController();
  final name = TextEditingController();
  final phone = TextEditingController();
  final gender = TextEditingController();
  final date = TextEditingController();
  final annual = TextEditingController();
  final address = TextEditingController();
  final locality = TextEditingController();
  final pincode = TextEditingController();
  final otp = TextEditingController();
  bool showspinner = false;
  // FormData formData;
  PickedFile _imageFile;
  final ImagePicker _picker = ImagePicker();
  // final _multiKey = GlobalKey<DropdownSearchState<String>>();
  // String email;
  // String password;
  //late String selectedGender;
  String selectedGender;
  String selectedState;
  String selectedDivision;
  String selectedDistrict;
  String selectedPolicestation;
  String stateId;
  String districtId;
  String divisionId;
  String policeStationId;

  //Variables for Sending Data to Server

  String emailData;
  String passwordData;
  String nameData;
  String phoneData;
  String dobData;
  String annualIncomeData;
  String addressData;
  String localityData;
  String otpData;
  String pincodeData;
  @override
  void initState() {
    _getStateList();
    // print(object)
    listenOtp();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF12375F),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text(
          'Citizen Registration',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: Theme(
        data: ThemeData(
            colorScheme: ColorScheme.light(primary: Color(0xFF12375F))),
        child: ModalProgressHUD(
          inAsyncCall: showspinner,
          child: Container(
            child: Column(
              children: [
                // Container(
                //   padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                //   child: Image.asset('assets/police.png'),
                //   height: 100,
                //   width: 100,
                // ),
                // SizedBox(
                //   height: 20.0,
                // ),
                // Text(
                //   'Citizen Registration',
                //   style: TextStyle(
                //     fontSize: 25,
                //     color: Color(0xFF12375F),
                //     fontWeight: FontWeight.w900,
                //     fontFamily: 'Poppins',
                //   ),
                // ),
                Expanded(
                  child: Stepper(
                    type: stepperType,
                    physics: ScrollPhysics(),
                    currentStep: _currentStep,
                    onStepTapped: (step) => tapped(step),
                    onStepContinue: continued,
                    onStepCancel: cancel,
                    steps: <Step>[
                      Step(
                        isActive: _currentStep >= 0,
                        state: _currentStep > 0
                            ? StepState.complete
                            : StepState.indexed,
                        title: new Text('Account'),
                        content: Form(
                          key: formKeys[0],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Email',
                                    labelText: 'Email'),
                                controller: email,
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (value) {
                                  emailData = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'This field is required';
                                  }
                                  if (!RegExp(
                                          r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                      .hasMatch(value)) {
                                    return "Please enter a valid email address";
                                  } else {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  }
                                  // the email is valid
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Enter Password',
                                      labelText: 'Password'),
                                  controller: password,
                                  keyboardType: TextInputType.visiblePassword,
                                  onChanged: (value) {
                                    passwordData = value;
                                  },
                                  obscureText: true,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'This field is required';
                                    } else {
                                      if (!RegExp(
                                              r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                                          .hasMatch(value)) {
                                        _showMyDialog('Invalid Password',
                                            'Your password must:\n\nContain at least:- \n>>8 characters.\n>>1 Digit.\n>>1 Uppercase Letter.\n>>1 Lowercase Letter.\n>>1 Special Character.');
                                        return 'Enter valid password';
                                      } else {
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());

                                        return null;
                                      }
                                    }
                                  }),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Enter Password',
                                      labelText: 'Confirm Password'),
                                  controller: confirmPassword,
                                  obscureText: true,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'This field is required';
                                    } else {
                                      if (value != password.text) {
                                        return 'Password do not match';
                                      } else {
                                        FocusScope.of(context)
                                            .requestFocus(new FocusNode());

                                        return null;
                                      }
                                    }
                                  }),
                            ],
                          ),
                        ),
                      ),
                      Step(
                        isActive: _currentStep >= 1,
                        state: _currentStep > 1
                            ? StepState.complete
                            : StepState.disabled,
                        title: new Text('Personal Details'),
                        content: Form(
                          key: formKeys[1],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Name',
                                    labelText: 'Name'),
                                controller: name,
                                onChanged: (value) {
                                  nameData = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'This field is required';
                                  } else if (value.length <= 2) {
                                    return "Invalid Name";
                                  } else {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  }
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Enter Phone Number',
                                      labelText: 'Phone'),
                                  controller: phone,
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    phoneData = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "This field is required";
                                    } else if (value.length != 10) {
                                      return "Invalid Phone Number";
                                    } else {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                    }
                                    return null;
                                  }),
                              SizedBox(
                                height: 25.0,
                              ),
                              DropdownSearch<String>(
                                //mode of dropdown
                                mode: Mode.MENU,
                                //to show search box
                                showSearchBox: false,
                                showSelectedItems: true,
                                maxHeight: 170,
                                onChanged: (value) {
                                  setState(() {
                                    selectedGender = value;
                                  });
                                },
                                selectedItem: selectedGender,
                                // onChanged: (value){
                                //   setState(() {
                                //     value=
                                //   });
                                // },
                                //list of dropdown items
                                items: [
                                  "Male",
                                  "Female",
                                  "Others",
                                ],
                                label: "Gender",
                                //show selected item
                                validator: (value) => value == null
                                    ? 'This field required'
                                    : null,
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                  controller: dateCtl,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: "Date of Birth",
                                  ),
                                  onTap: () async {
                                    DateTime cdate = DateTime.now();
                                    DateTime firstDate = DateTime(
                                        cdate.year - 150,
                                        cdate.month,
                                        cdate.day);
                                    DateTime lastDate = DateTime(
                                      cdate.year - 5,
                                      cdate.month,
                                      cdate.day,
                                    );
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    DateTime date = (await showDatePicker(
                                      context: context,
                                      initialDate: lastDate,
                                      firstDate: firstDate,
                                      lastDate: lastDate,
                                    ));

                                    var formate1 =
                                        "${date.year}-${date.month}-${date.day}";
                                    dateCtl.text = formate1;
                                    dobData = formate1;
                                    print(dobData);
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "This field is required";
                                    }
                                    return null;
                                  }),
                            ],
                          ),
                        ),
                      ),
                      Step(
                        title: new Text('Address'),
                        content: Form(
                          key: formKeys[2],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: null,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      isDense: true,
                                      contentPadding:
                                          EdgeInsets.fromLTRB(10, 10, 10, 30),
                                      hintText: 'Enter Your Address ',
                                      labelText: 'Address'),
                                  controller: address,
                                  onChanged: (value) {
                                    addressData = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "This field is required";
                                    } else if (value.length < 10) {
                                      return "Invalid Address";
                                    } else {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                    }
                                    return null;
                                  }),
                              SizedBox(
                                height: 20.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Locality',
                                    labelText: 'Locality'),
                                controller: locality,
                                onChanged: (value) {
                                  localityData = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "This field is required";
                                  } else if (value.length <= 2) {
                                    return "Invalid Locality";
                                  } else {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                  }
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      hintText: 'Enter Pincode  ',
                                      labelText: 'Pincode'),
                                  controller: pincode,
                                  keyboardType: TextInputType.number,
                                  onChanged: (value) {
                                    pincodeData = value;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "This field is required";
                                    } else if (value.length != 6) {
                                      return "Invalid Pincode";
                                    } else {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                    }
                                    return null;
                                  }),
                            ],
                          ),
                        ),
                        isActive: _currentStep >= 2,
                        state: _currentStep > 2
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text('NPS'),
                        content: Form(
                          key: formKeys[3],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _myState,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select State',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                // print(newValue);
                                                _myState = newValue;
                                                _getDistrictsList();
                                                print(_myState);
                                              });
                                            },
                                            items: statesList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mydistrict,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select District',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mydistrict = newValue;
                                                _getDivisionsList();
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict);
                                              });
                                            },
                                            items: districtList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mydivision,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select Division',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mydivision = newValue;
                                                _getPoliceStationList();
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict +
                                                    ' ' +
                                                    _mydivision);
                                              });
                                            },
                                            items: divisionList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mypolicestation,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select Police Station',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mypolicestation = newValue;
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict +
                                                    ' ' +
                                                    _mydivision +
                                                    ' ' +
                                                    _mypolicestation);
                                              });
                                            },
                                            items:
                                                policeStationList?.map((item) {
                                                      return new DropdownMenuItem(
                                                        child: new Text(item),
                                                        value: item.toString(),
                                                      );
                                                    })?.toList() ??
                                                    [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        isActive: _currentStep >= 3,
                        state: _currentStep > 3
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text('Image Upload'),
                        content: Form(
                          key: formKeys[4],
                          child: Column(
                            children: <Widget>[
                              Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 40.0),
                                    child: Center(
                                      child: ClipOval(
                                        child: BackdropFilter(
                                          filter: ImageFilter.blur(
                                              sigmaX: 3, sigmaY: 3),
                                          child: CircleAvatar(
                                            radius: size.width * 0.16,
                                            backgroundColor:
                                                Colors.grey[400].withOpacity(
                                              0.4,
                                            ),
                                            backgroundImage: _imageFile == null
                                                ? NetworkImage(
                                                    'https://t3.ftcdn.net/jpg/02/70/09/98/240_F_270099822_9zbx236dHn1hyxYNl9HSOBvpUEpU0eOz.jpg',
                                                  )
                                                : FileImage(
                                                    File(_imageFile.path),
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: size.height * 0.10,
                                    left: size.width * 0.40,
                                    child: Container(
                                      height: size.width * 0.1,
                                      width: size.width * 0.1,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        shape: BoxShape.circle,
                                        border: Border.all(width: 0.5),
                                      ),
                                      child: RoundIconButton(
                                        onTap: () async {
                                          final pickedfile =
                                              await _picker.getImage(
                                            source: ImageSource.camera,
                                          );
                                          setState(() {
                                            _imageFile = pickedfile;
                                          });
                                        },
                                        icon: Icons.upload,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        isActive: _currentStep >= 4,
                        state: _currentStep > 4
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text('Verification'),
                        content: Form(
                          key: formKeys[5],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                margin:
                                    EdgeInsets.only(left: 10.0, right: 10.0),
                                padding: EdgeInsets.all(5.0),
                                child: PinFieldAutoFill(
                                  decoration: UnderlineDecoration(
                                    colorBuilder: FixedColorBuilder(
                                      Color(0xFF12375F),
                                    ),
                                  ),
                                  // focusNode: Focus.of(context).,
                                  controller: otp,
                                  codeLength: 6,
                                  onCodeChanged: (value) {
                                    if (value.length == 6) {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                    }
                                    otpData = value;
                                  },
                                  currentCode: otpData,
                                  onCodeSubmitted: (value) {
                                    // if (value.length != null) {
                                    //   FocusScope.of(context)
                                    //       .requestFocus(FocusNode());
                                    // }
                                    if (value.length == 6) {
                                      onSubmit(value);
                                      // print(value);
                                      otpData = value;
                                    }
                                    print(value);
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                            ],
                          ),
                        ),
                        isActive: _currentStep >= 5,
                        state: _currentStep > 5
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                    ],
                    controlsBuilder: (context, {onStepContinue, onStepCancel}) {
                      final isOtpStep = _currentStep == 4;
                      final isLastStep = _currentStep == 5;
                      final isFirstScreen = _currentStep == 0;
                      return Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Row(
                          children: [
                            Container(
                              height: 35,
                              width: MediaQuery.of(context).size.width * 0.30,
                              child: ElevatedButton(
                                child: Text('CLEAR'),
                                onPressed: (isOtpStep || isLastStep)
                                    ? null
                                    : onStepCancel,
                              ),
                            ),
                            const SizedBox(width: 12),
                            Container(
                              height: 35,
                              width: MediaQuery.of(context).size.width * 0.30,
                              child: ElevatedButton(
                                child: Text(
                                  isOtpStep
                                      ? 'Send OTP'
                                      : isLastStep
                                          ? 'Submit'
                                          : 'NEXT',
                                  style: TextStyle(
                                    fontSize: 15.0,
                                  ),
                                ),
                                onPressed: onStepContinue,
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    if (formKeys[_currentStep].currentState.validate()) {
      setState(() {
        if (_currentStep == 5 && otpData.length == 6) {
          onSubmit(otpData);
        }
        if (_currentStep == 4 && _imageFile != null) {
          // print('completed');
          // _currentStep += 1;
          onSend();
        } else if (_currentStep == 3 &&
            (_myState == null ||
                _mydistrict == null ||
                _mydivision == null ||
                _mypolicestation == null)) {
          _currentStep = 3;
          if (_myState == null) {
            _showMyDialog('State is Empty', 'Choose the state from dropdown');
          } else if (_mydistrict == null) {
            _showMyDialog(
                'District is Empty', 'Choose the district from dropdown');
          } else if (_mydivision == null) {
            _showMyDialog(
                'Division is Empty', 'Choose the division from dropdown');
          } else if (_mypolicestation == null) {
            _showMyDialog(
                'Police Station is Empty', 'Choose the PS from dropdown');
          }
        } else if (_currentStep == 4 && _imageFile == null) {
          _currentStep = 4;
          if (_imageFile == null) {
            _showMyDialog('Image is Empty', 'Capture Account Holder Image');
          }
          print(_currentStep);
        } else if (_currentStep < 5) {
          _currentStep += 1;
        }
      });
      print(_currentStep);
    }
  }

  cancel() {
    setState(() {
      if (_currentStep == 0) {
        email.clear();
        password.clear();
        confirmPassword.clear();
      } else if (_currentStep == 1) {
        phone.clear();
        name.clear();
        gender.clear();
        dateCtl.clear();
        annual.clear();
        selectedGender = null;
      } else if (_currentStep == 2) {
        address.clear();
        locality.clear();
        pincode.clear();
      } else if (_currentStep == 3) {
        _myState = null;
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        // formKeys[3].currentState.reset();
      }
    });
  }

  void listenOtp() async {
    await SmsAutoFill().listenForCode;
  }

  onSend() async {
    //_currentStep += 1;
    // policeStationId = getPolicestationId(_mypolicestation);
    // print(emailData);
    // print(passwordData);
    // print(nameData);
    // print(phoneData);
    // print(selectedGender);
    // print(dobData);
    // print(annualIncomeData);
    // print(addressData);
    // print(localityData);
    // print(pincodeData);
    // print(policeStationId);
    //print(_imageFile.path);
    setState(() {
      showspinner = true;
    });
    policeStationId = getPolicestationId(_mypolicestation);
    var uri = Uri.parse('$url/auth/citizen/register');
    var request = http.MultipartRequest('POST', uri);
    request.fields['email'] = emailData;
    request.fields['password'] = passwordData;
    request.fields['name'] = nameData;
    request.fields['phone'] = phoneData;
    request.fields['gender'] = selectedGender;
    request.fields['dob'] = dobData;
    request.fields['address'] = addressData;
    request.fields['locality'] = localityData;
    request.fields['pincode'] = pincodeData;
    request.fields['policeStation'] = policeStationId;
    request.files.add(await http.MultipartFile.fromPath(
        'image', _imageFile.path,
        filename: 'profile.jpg', contentType: MediaType('image', 'jpg')));
    var response = await request.send();
    if (response.statusCode == 200 || response.statusCode == 201) {
      setState(() {
        showspinner = false;
      });
      _currentStep += 1;
    } else {
      setState(() {
        showspinner = false;
      });
      String errorCause = await response.stream.bytesToString();
      _showMyDialog('Error: ${response.statusCode}', errorCause);
    }
    print('Response=' + await response.stream.bytesToString());
  }

  onSubmit(String code) async {
    print("Hello Bulbul Mam -" + code);
    setState(() {
      showspinner = true;
    });
    http.Response response = await registerotpverify(code, emailData);
    setState(() {
      showspinner = false;
    });
    var data = response.body;
    var decodedData = jsonDecode(data);
    print(decodedData.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      setState(() {
        showspinner = false;
      });
      String sType = "Account";
      Navigator.popAndPushNamed(context, 'SuccessScreen',
          arguments: {'successType': sType});
      //Redirect to Success Screen
      print('Registered Successfully');
    } else {
      setState(() {
        showspinner = false;
      });
      _showMyDialog('${response.statusCode}', decodedData['message']);
    }
    // Required Code..........////
  }

  //=============================================================================== Api Calling here

//CALLING STATE API HERE
// Get State information by API
  List statesList = [];
  List statesIdList = [];
  String _myState;
  String _myStateId;

  Future<String> _getStateList() async {
    setState(() {
      showspinner = true;
    });
    String stateInfoUrl = '$url/location';
    await http.get(stateInfoUrl).then((response) {
      List data = jsonDecode(response.body);
      int i;
      // print(data);
      setState(() {
        _myState = null;
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          statesList.add(data[i]['name']);
          statesIdList.add(data[i]['_id']);
        }
        showspinner = false;
        // print(statesList);
      });
    });
  }

  // Get District information by API
  List districtList = [];
  List districtIdList = [];
  String _mydistrict;

  Future<String> _getDistrictsList() async {
    setState(() {
      showspinner = true;
    });
    print('Hello');
    int i;
    for (i = 0; i < statesList.length; i++) {
      if (statesList[i] == _myState) {
        stateId = statesIdList[i];
        break;
      }
    }
    String districtInfoUrl = '$url/location?state=$stateId';
    await http.get(districtInfoUrl).then((response) {
      List data = jsonDecode(response.body);
      setState(() {
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          districtList.add(data[i]['name']);
          districtIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  // Get Division information by API
  List divisionList = [];
  List divisionIdList = [];
  String _mydivision;

  Future<String> _getDivisionsList() async {
    setState(() {
      showspinner = true;
    });
    int i;
    for (i = 0; i < districtList.length; i++) {
      if (districtList[i] == _mydistrict) {
        districtId = districtIdList[i];
        break;
      }
    }
    String divisionInfoUrl =
        '$url/location?state=$stateId&district=$districtId';
    await http.get(divisionInfoUrl).then((response) {
      List data = json.decode(response.body);

      setState(() {
        // divisionList = data['divisions'];
        _mydivision = null;
        _mypolicestation = null;
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          divisionList.add(data[i]['name']);
          divisionIdList.add(data[i]['_id']);
        }
        showspinner = false;
        // print(divisionList);
      });
    });
  }

  // Get Police Station information by API
  List policeStationList = [];
  List policeStationIdList = [];
  String _mypolicestation;

  Future<String> _getPoliceStationList() async {
    setState(() {
      showspinner = true;
    });
    int i;
    for (i = 0; i < divisionList.length; i++) {
      if (divisionList[i] == _mydivision) {
        divisionId = divisionIdList[i];
        break;
      }
    }
    String policeStationUrl =
        '$url/location?state=$stateId&district=$districtId&division=$divisionId';
    await http.get(policeStationUrl).then((response) {
      List data = json.decode(response.body);

      setState(() {
        _mypolicestation = null;
        policeStationList.clear();
        policeStationIdList.clear();
        // policeStationList = data['policeStations'];
        for (i = 0; i < data.length; i++) {
          policeStationList.add(data[i]['name']);
          policeStationIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  String getPolicestationId(String policeStation) {
    int i;
    String policeStationId;
    for (i = 0; i < policeStationList.length; i++) {
      if (policeStationList[i] == policeStation) {
        policeStationId = policeStationIdList[i];
        break;
      }
    }
    return policeStationId;
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
