import 'package:CRS/service/constant.dart';
import 'package:CRS/service/variables.dart';
import 'package:flutter/material.dart';
import 'package:CRS/service/complaint.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:CRS/service/user_auth.dart';

class ComplaintStatus extends StatefulWidget {
  @override
  _ComplaintStatusState createState() => _ComplaintStatusState();
}

class _ComplaintStatusState extends State<ComplaintStatus> {
  String status;
  String date;
  String id;
  List complains;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: kBlue,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: kWhite,
          ),
        ),
        title: Text(
          'Compalint History',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: getComplaintList(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                complains.length != 0) {
              return ListView(
                children: complain(complains, Variables.token),
              );
            } else if (snapshot.connectionState == ConnectionState.done &&
                complains.length == 0) {
              return Center(
                child: Text(
                  'No Complaints Found',
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontSize: 20.0,
                  ),
                ),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  Future getComplaintList() async {
    http.Response response = await complaints(Variables.token);
    dynamic data = response.body;
    print(data.toString());
    dynamic variable = jsonDecode(data);
    if (response.statusCode == 200) {
      complains = jsonDecode(data);
      //complains.clear();
    } else {
      _showMyDialog('Error: ${response.statusCode}', variable['message']);
    }
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

List<Widget> complain(List complaint, String token) {
  List<Widget> clist = [];
  for (var i = 0; i < complaint.length; i++) {
    clist.add(
      Complaint(
        date: complaint[i]['createdAt'],
        id: complaint[i]['_id'],
        progress: complaint[i]['progress'],
        title: complaint[i]['title'],
        token: token,
      ),
    );
  }
  return clist;
}
