import 'package:CRS/service/variables.dart';
import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: kBlue,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: kWhite,
          ),
        ),
        title: Text(
          'Profile',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Column(
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                CircleAvatar(
                  radius: 65.0,
                  backgroundColor: Colors.blue.shade300,
                  backgroundImage: NetworkImage(
                    '${Variables.image}',
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Text(
                  Variables.name,
                  style: TextStyle(
                    color: Colors.grey.shade700,
                    fontSize: 35.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PTSans',
                  ),
                ),
                Text(
                  Variables.email,
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey.shade700,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                  width: 150.0,
                  child: Divider(
                    color: Colors.teal[900],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Card(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'Mobile No:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.phone,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'Address:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.address,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'Pin Code:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      '${Variables.pinCode}',
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'State:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.state,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'District:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.district,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'Division:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.division,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
                Card(
                  margin:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    title: Text(
                      'Police Station:',
                      style: profileheadingTextStyle,
                    ),
                    subtitle: Text(
                      Variables.policeStation,
                      style: profileBodyTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
