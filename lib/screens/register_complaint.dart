import 'dart:ui';
import 'package:CRS/service/location.dart';
import 'package:CRS/service/task_data.dart';
import 'package:CRS/service/task_item.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:date_time_picker/date_time_picker.dart';
import 'package:CRS/service/user_auth.dart';
import 'package:provider/provider.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:dropdown_search/dropdown_search.dart';

List<GlobalKey<FormState>> formKeys = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

class RegisterComplaint extends StatefulWidget {
  @override
  _RegisterComplaintState createState() => _RegisterComplaintState();
}

class _RegisterComplaintState extends State<RegisterComplaint> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;
  TextEditingController dateCtl = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final ctitle = TextEditingController();
  final cdateTime = TextEditingController();
  final clocation = TextEditingController();
  final cdescription = TextEditingController();
  final vname = TextEditingController();
  final vphone = TextEditingController();
  final vdetails = TextEditingController();
  final vDob = TextEditingController();
  final aname = TextEditingController();
  final aAge = TextEditingController();
  final aDetails = TextEditingController();

  bool showspinner = false;
  String selectedGenderVictim;
  String selectedGenderAccused;
  String selectedCrimeType;
  String selectedState;
  String selectedDivision;
  String selectedDistrict;
  String selectedPolicestation;
  String stateId;
  String districtId;
  String divisionId;
  String policeStationId;

  //Variables for Sending Data to Server
  String dTime;
  int dobData = -1;
  String crimetitle;
  String crimeLocation;
  String crimeDescription;
  String victimName;
  String victimPhone;
  String victimDetails;
  String accusedName;
  int accusedAge = -1;
  String accusedDetails;
  @override
  void initState() {
    _getStateList();
    getLocation();
    super.initState();
  }

  void getLocation() async {
    await Location().getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF12375F),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text(
          'Complaint Registration',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: Theme(
        data: ThemeData(
            colorScheme: ColorScheme.light(primary: Color(0xFF12375F))),
        child: ModalProgressHUD(
          inAsyncCall: showspinner,
          child: Container(
            child: Column(
              children: [
                Expanded(
                  child: Stepper(
                    type: stepperType,
                    physics: ScrollPhysics(),
                    currentStep: _currentStep,
                    onStepTapped: (step) => tapped(step),
                    onStepContinue: continued,
                    onStepCancel: cancel,
                    steps: <Step>[
                      Step(
                        isActive: _currentStep >= 0,
                        state: _currentStep > 0
                            ? StepState.complete
                            : StepState.indexed,
                        title: new Text('Complaint Details'),
                        content: Form(
                          key: formKeys[0],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Title',
                                    labelText: 'Title'),
                                controller: ctitle,
                                keyboardType: TextInputType.text,
                                onChanged: (value) {
                                  crimetitle = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'This field is required';
                                  }
                                  if (value.length < 15) {
                                    return "Title should be minimum 15 Characters";
                                  }
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              DropdownSearch<String>(
                                mode: Mode.MENU,
                                showSearchBox: false,
                                showSelectedItems: true,
                                maxHeight: 170,
                                onChanged: (value) {
                                  setState(() {
                                    selectedCrimeType = value;
                                  });
                                },
                                selectedItem: selectedCrimeType,
                                items: [
                                  "DRUG",
                                  "RAPE",
                                  "MURDER",
                                  "THEFT",
                                  "CYBER CRIME",
                                  "ASSUALT",
                                  "CIVIL",
                                  "ROBBERY",
                                  "DOMESTIC VIOLENCE",
                                  "VIOLENCE",
                                  "OTHERS",
                                ],
                                label: "Type",
                                validator: (value) => value == null
                                    ? 'This field required'
                                    : null,
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                ),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: DateTimePicker(
                                  type: DateTimePickerType.dateTime,
                                  dateMask: 'd MMM, yyyy',
                                  initialValue: null,
                                  firstDate: DateTime(1900),
                                  lastDate: DateTime.now(),
                                  dateLabelText: 'Date & Time',
                                  controller: cdateTime,
                                  onChanged: (val) {
                                    dTime =
                                        DateTime.parse(val).toIso8601String();
                                    print(dTime);
                                  },
                                  validator: (value) => value.length == 0
                                      ? 'This field required'
                                      : null,
                                ),
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Enter Location ',
                                  labelText: 'Location',
                                ),
                                controller: clocation,
                                keyboardType: TextInputType.text,
                                onChanged: (value) {
                                  crimeLocation = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "This field is required";
                                  } else if (value.length < 10) {
                                    return "Location must be minimum 10 Characters";
                                  }
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  return null;
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    isDense: true,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 30),
                                    hintText: 'Enter Description',
                                    labelText: 'Description'),
                                controller: cdescription,
                                onChanged: (value) {
                                  crimeDescription = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "This field is required";
                                  } else if (value.length < 10) {
                                    return "Invalid Address";
                                  }
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());

                                  return null;
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Step(
                        isActive: _currentStep >= 1,
                        state: _currentStep > 1
                            ? StepState.complete
                            : StepState.disabled,
                        title: new Text('Victim Details'),
                        content: Form(
                          key: formKeys[1],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.name,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Victim Name',
                                    labelText: 'Name'),
                                controller: vname,
                                onChanged: (value) {
                                  victimName = value;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    isDense: true,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 30),
                                    hintText: 'Enter Victim Details',
                                    labelText: 'Details'),
                                controller: vdetails,
                                onChanged: (value) {
                                  victimDetails = value;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              DropdownSearch<String>(
                                mode: Mode.MENU,
                                showSearchBox: false,
                                showSelectedItems: true,
                                maxHeight: 170,
                                onChanged: (value) {
                                  setState(() {
                                    selectedGenderVictim = value;
                                  });
                                },
                                selectedItem: selectedGenderVictim,
                                items: [
                                  "Male",
                                  "Female",
                                  "Others",
                                ],
                                label: "Gender",
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Victim Age',
                                    labelText: 'Age'),
                                controller: vDob,
                                onChanged: (value) {
                                  dobData = int.parse(value);
                                },
                                validator: (value) {
                                  if (value.length > 4) {
                                    return 'Invalid Victim Age';
                                  }
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  return null;
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Step(
                        isActive: _currentStep >= 2,
                        state: _currentStep > 2
                            ? StepState.complete
                            : StepState.disabled,
                        title: new Text('Accused Details'),
                        content: Form(
                          key: formKeys[2],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Accused Name',
                                    labelText: 'Accused Name'),
                                controller: aname,
                                onChanged: (value) {
                                  accusedName = value;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText:
                                        'Enter Approximate Age of Accused',
                                    labelText: 'Age'),
                                controller: aAge,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  accusedAge = int.parse(value);
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    isDense: true,
                                    contentPadding:
                                        EdgeInsets.fromLTRB(10, 10, 10, 30),
                                    hintText: 'Enter Accused Details',
                                    labelText: 'Accused Details'),
                                controller: aDetails,
                                onChanged: (value) {
                                  accusedDetails = value;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                              ),
                              SizedBox(
                                height: 25.0,
                              ),
                              DropdownSearch<String>(
                                mode: Mode.MENU,
                                showSearchBox: false,
                                showSelectedItems: true,
                                maxHeight: 170,
                                onChanged: (value) {
                                  setState(() {
                                    selectedGenderAccused = value;
                                  });
                                },
                                selectedItem: selectedGenderAccused,
                                items: [
                                  "Male",
                                  "Female",
                                  "Others",
                                ],
                                label: "Accused Gender",
                              ),
                            ],
                          ),
                        ),
                      ),
                      Step(
                        title: new Text('NPS'),
                        content: Form(
                          key: formKeys[3],
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _myState,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select State',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _myState = newValue;
                                                _getDistrictsList();
                                                print(_myState);
                                              });
                                            },
                                            items: statesList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mydistrict,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select District',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mydistrict = newValue;
                                                _getDivisionsList();
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict);
                                              });
                                            },
                                            items: districtList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mydivision,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select Division',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mydivision = newValue;
                                                _getPoliceStationList();
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict +
                                                    ' ' +
                                                    _mydivision);
                                              });
                                            },
                                            items: divisionList?.map((item) {
                                                  return new DropdownMenuItem(
                                                    child: new Text(item),
                                                    value: item.toString(),
                                                  );
                                                })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1.0,
                                    color: Colors.grey.shade500,
                                  ),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: _mypolicestation,
                                            iconSize: 30,
                                            icon: (null),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select Police Station',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _mypolicestation = newValue;
                                                print(_myState +
                                                    ' ' +
                                                    _mydistrict +
                                                    ' ' +
                                                    _mydivision +
                                                    ' ' +
                                                    _mypolicestation);
                                              });
                                            },
                                            items:
                                                policeStationList?.map((item) {
                                                      return new DropdownMenuItem(
                                                        child: new Text(item),
                                                        value: item.toString(),
                                                      );
                                                    })?.toList() ??
                                                    [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        isActive: _currentStep >= 3,
                        state: _currentStep > 3
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        isActive: _currentStep >= 4,
                        state: _currentStep > 4
                            ? StepState.complete
                            : StepState.disabled,
                        title: new Text('Upload Files'),
                        content: Form(
                          key: formKeys[4],
                          child: SingleChildScrollView(
                            child: SizedBox(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              child: TaskItem(),
                            ),
                          ),
                        ),
                      )
                    ],
                    controlsBuilder: (context, {onStepContinue, onStepCancel}) {
                      final isLastStep = _currentStep == 4;
                      final isFirstScreen = _currentStep == 0;
                      return Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Row(
                          children: [
                            Container(
                              height: 35,
                              width: MediaQuery.of(context).size.width * 0.25,
                              child: ElevatedButton(
                                  child: Text(
                                    isLastStep ? 'Add Files' : 'CLear',
                                  ),
                                  onPressed: () {
                                    if (isLastStep) {
                                      Provider.of<TaskListData>(context,
                                              listen: false)
                                          .addFiles();
                                    } else {
                                      cancel();
                                    }
                                  }),
                            ),
                            const SizedBox(width: 12),
                            Container(
                              height: 35,
                              width: MediaQuery.of(context).size.width * 0.25,
                              child: ElevatedButton(
                                child: Text(
                                  isLastStep ? 'Submit' : 'NEXT',
                                  style: TextStyle(
                                    fontSize: 15.0,
                                  ),
                                ),
                                onPressed: onStepContinue,
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    if (formKeys[_currentStep].currentState.validate()) {
      setState(() {
        if (_currentStep == 3 &&
            (_myState == null ||
                _mydistrict == null ||
                _mydivision == null ||
                _mypolicestation == null)) {
          _currentStep = 3;
          if (_myState == null) {
            _showMyDialog('State is Empty', 'Choose the state from dropdown');
          } else if (_mydistrict == null) {
            _showMyDialog(
                'District is Empty', 'Choose the district from dropdown');
          } else if (_mydivision == null) {
            _showMyDialog(
                'Division is Empty', 'Choose the division from dropdown');
          } else if (_mypolicestation == null) {
            _showMyDialog(
                'Police Station is Empty', 'Choose the PS from dropdown');
          }
        } else if (_currentStep < 4) {
          _currentStep += 1;
        } else if (_currentStep == 4) {
          onSubmit();
        }
      });
      print(_currentStep);
    }
  }

  cancel() {
    setState(() {
      if (_currentStep == 0) {
        ctitle.clear();
        selectedCrimeType = null;
        cdateTime.clear();
        clocation.clear();
        cdescription.clear();
      } else if (_currentStep == 1) {
        vname.clear();
        vphone.clear();
        vdetails.clear();
        vDob.clear();
        dobData = null;
        selectedGenderVictim = null;
      } else if (_currentStep == 2) {
        aname.clear();
        aAge.clear();
        aDetails.clear();
        selectedGenderAccused = null;
      } else if (_currentStep == 3) {
        _myState = null;
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
      }
    });
  }

  onSubmit() async {
    setState(() {
      showspinner = true;
    });
    policeStationId = getPolicestationId(_mypolicestation);
    Map victim = Map();
    Map accused = Map();
    if (victimName != null && victimName.isNotEmpty) {
      victim['name'] = victimName;
    }
    if (!dobData.isNegative) {
      victim['age'] = dobData;
    }
    if (victimDetails != null && victimDetails.isNotEmpty) {
      victim['details'] = victimDetails;
    }
    if (selectedGenderVictim != null && selectedGenderVictim.isNotEmpty) {
      victim['gender'] = selectedGenderVictim[0];
    }
    if (accusedName != null && accusedName.isNotEmpty) {
      accused['name'] = accusedName;
    }
    if (!accusedAge.isNegative) {
      accused['age'] = accusedAge;
    }
    if (accusedDetails != null && accusedDetails.isNotEmpty) {
      accused['details'] = accusedDetails;
    }
    if (selectedGenderAccused != null && selectedGenderAccused.isNotEmpty) {
      accused['gender'] = selectedGenderAccused[0];
    }

    Map gotresponse =
        await Provider.of<TaskListData>(context, listen: false).send(
      crimetitle,
      selectedCrimeType,
      dTime,
      crimeLocation,
      crimeDescription,
      victim,
      accused,
      Location.userLocation,
      policeStationId,
    );
    if (gotresponse['statusCode'] == 200 || gotresponse['statusCode'] == 201) {
      setState(() {
        showspinner = false;
      });
      String sType = "Complaint";
      Navigator.popAndPushNamed(context, 'SuccessScreen',
          arguments: {'successType': sType});
      //Redirect to Success Screen
      print('Registered Successfully');
    } else {
      setState(() {
        showspinner = false;
      });
      _showMyDialog(gotresponse['message'], '${gotresponse['statusCode']}');
    }
    // Required Code..........////
  }

  //=============================================================================== Api Calling here

//CALLING STATE API HERE
// Get State information by API
  List statesList = [];
  List statesIdList = [];
  String _myState;
  String _myStateId;

  Future<String> _getStateList() async {
    setState(() {
      showspinner = true;
    });
    String stateInfoUrl = '$url/location';
    await http.get(stateInfoUrl).then((response) {
      List data = jsonDecode(response.body);
      int i;
      // print(data);
      setState(() {
        _myState = null;
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          statesList.add(data[i]['name']);
          statesIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  // Get District information by API
  List districtList = [];
  List districtIdList = [];
  String _mydistrict;

  Future<String> _getDistrictsList() async {
    setState(() {
      showspinner = true;
    });
    print('Hello');
    int i;
    for (i = 0; i < statesList.length; i++) {
      if (statesList[i] == _myState) {
        stateId = statesIdList[i];
        break;
      }
    }
    String districtInfoUrl = '$url/location?state=$stateId';
    await http.get(districtInfoUrl).then((response) {
      List data = jsonDecode(response.body);
      setState(() {
        _mydistrict = null;
        _mydivision = null;
        _mypolicestation = null;
        districtList.clear();
        districtIdList.clear();
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          districtList.add(data[i]['name']);
          districtIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  // Get Division information by API
  List divisionList = [];
  List divisionIdList = [];
  String _mydivision;

  Future<String> _getDivisionsList() async {
    setState(() {
      showspinner = true;
    });
    int i;
    for (i = 0; i < districtList.length; i++) {
      if (districtList[i] == _mydistrict) {
        districtId = districtIdList[i];
        break;
      }
    }
    String divisionInfoUrl =
        '$url/location?state=$stateId&district=$districtId';
    await http.get(divisionInfoUrl).then((response) {
      List data = json.decode(response.body);

      setState(() {
        _mydivision = null;
        _mypolicestation = null;
        divisionList.clear();
        divisionIdList.clear();
        policeStationList.clear();
        policeStationIdList.clear();
        for (i = 0; i < data.length; i++) {
          divisionList.add(data[i]['name']);
          divisionIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  // Get Police Station information by API
  List policeStationList = [];
  List policeStationIdList = [];
  String _mypolicestation;

  Future<String> _getPoliceStationList() async {
    setState(() {
      showspinner = true;
    });
    int i;
    for (i = 0; i < divisionList.length; i++) {
      if (divisionList[i] == _mydivision) {
        divisionId = divisionIdList[i];
        break;
      }
    }
    String policeStationUrl =
        '$url/location?state=$stateId&district=$districtId&division=$divisionId';
    await http.get(policeStationUrl).then((response) {
      List data = json.decode(response.body);

      setState(() {
        _mypolicestation = null;
        policeStationList.clear();
        policeStationIdList.clear();

        for (i = 0; i < data.length; i++) {
          policeStationList.add(data[i]['name']);
          policeStationIdList.add(data[i]['_id']);
        }
        showspinner = false;
      });
    });
  }

  String getPolicestationId(String policeStation) {
    int i;
    String policeStationId;
    for (i = 0; i < policeStationList.length; i++) {
      if (policeStationList[i] == policeStation) {
        policeStationId = policeStationIdList[i];
        break;
      }
    }
    return policeStationId;
  }

  // Alert
  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
