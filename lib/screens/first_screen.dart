import 'package:CRS/service/bottom_nav_button.dart';
import 'package:CRS/service/carousel.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kBlue,
        elevation: 0,
        leading: Container(
          margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0),
          child: Image(
            image: AssetImage('images/police.png'),
          ),
        ),
        title: Text(
          'Talaash',
          style: TextStyle(
            fontSize: 25,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          CarouSEL(),
          SizedBox(
            height: 5.0,
          ),
          Center(
            child: TyperAnimatedTextKit(
              textAlign: TextAlign.center,
              text: [
                "Disclaimer - The information contained in this Mobile App is provided for informational purposes only"
              ],
              textStyle: TextStyle(
                  color: Color(0xFF0D47A1),
                  fontFamily: 'Poppins',
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 100.0, right: 100.0),
            width: MediaQuery.of(context).size.width,
            child: Divider(
              color: Color(0xFF12375F),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            margin: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Text(
              'NCRB was set-up in 1986 to function as a repository of information on crime and criminals so as to assist the investigators in linking crime to the perpetrators based on the recommendations of the Tandon Committee, National Police Commission (1977-1981) and the MHA’s Task force (1985).Subsequently, NCRB was entrusted with the responsibility for monitoring, coordinating and implementing the Crime and Criminal Tracking Network & Systems (CCTNS) project in the year 2009. The project has connected 15000+ police stations and 6000 higher offices of police in the.',
              style: firstScreenDescriptionStyle,
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 30.0),
        child: Row(
          children: [
            BottomNavButton(
              buttonName: 'Login',
              routeName: 'login',
            ),
            SizedBox(
              width: 10.0,
            ),
            BottomNavButton(
              buttonName: 'Register',
              routeName: 'CreateNewAccount',
            ),
          ],
        ),
      ),
    );
  }
}
