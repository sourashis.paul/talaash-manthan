import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:CRS/service/constant.dart';
import 'package:CRS/service/rounded-button.dart';
import 'package:CRS/service/user_auth.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  String email;
  bool showspinner = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ModalProgressHUD(
      inAsyncCall: showspinner,
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: kBlue,
              elevation: 0,
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: kWhite,
                ),
              ),
              title: Text(
                'Forgot Password',
                style: TextStyle(
                  fontSize: 22,
                  color: Colors.white,
                  height: 1.5,
                  fontFamily: 'PTSans',
                ),
              ),
              centerTitle: true,
            ),
            body: Column(
              children: [
                Center(
                  child: Column(
                    children: [
                      SizedBox(
                        height: size.height * 0.1,
                      ),
                      Container(
                        width: size.width * 0.9,
                        child: Text(
                          'Enter your registered email to reset your password',
                          style: kBodyText,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 25.0, right: 25.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Enter Email',
                              labelText: 'Email'),
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) {
                            email = value;
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'This field is required';
                            }
                            if (!RegExp(
                                    r'^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
                                .hasMatch(value)) {
                              return "Please enter a valid email address";
                            } else {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                            }
                            // the email is valid
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        buttonName: 'Send',
                        ontap: () async {
                          setState(() {
                            showspinner = true;
                          });
                          if (email == null) {
                            setState(() {
                              showspinner = false;
                            });
                            Alert(
                              context: context,
                              title: 'Please Fill the Form!',
                              desc: 'Invalid E-mail',
                            ).show();
                          } else {
                            http.Response response =
                                await citizenforgotpassword(email);
                            String data = response.body;
                            dynamic decodedData = jsonDecode(data);
                            // showspinner = true;
                            if (response.statusCode == 200 ||
                                response.statusCode == 201) {
                              setState(() {
                                showspinner = false;
                              });
                              _showMyDialog('Sucessfully Email Sent',
                                  decodedData['message']);
                            } else {
                              _showMyDialog('Error: ${response.statusCode}',
                                  decodedData['message']);
                            }
                            setState(() {
                              showspinner = false;
                            });
                          }
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
