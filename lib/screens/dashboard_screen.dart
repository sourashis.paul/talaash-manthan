import 'dart:convert';
import 'package:CRS/screens/emergency_screen.dart';
import 'package:CRS/service/location.dart';
import 'package:CRS/service/variables.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:CRS/screens/complaint_status.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:CRS/screens/register_complaint.dart';
import 'package:CRS/screens/profile.dart';
import 'profile.dart';
import 'package:CRS/service/constant.dart';
import 'package:CRS/service/dashboard_button.dart';

class DashBoard extends StatefulWidget {
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  bool showspinner = false;
  @override
  void initState() {
    getLocation();
    super.initState();
  }

  void getLocation() async {
    await Location().getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: ModalProgressHUD(
        inAsyncCall: showspinner,
        child: Scaffold(
          backgroundColor: kBlue,
          body: SafeArea(
            child: Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.topRight,
                  child: TextButton(
                    onPressed: () {
                      _onLogout(context);
                    },
                    child: Icon(
                      FontAwesomeIcons.powerOff,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                CircleAvatar(
                  radius: 65.0,
                  backgroundColor: Colors.blue.shade300,
                  backgroundImage: NetworkImage(
                    '${Variables.image}',
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Text(
                  Variables.name,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PTSans',
                  ),
                ),
                Text(
                  Variables.email,
                  style: TextStyle(
                    fontFamily: 'PTSans',
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                  width: 150.0,
                  child: Divider(
                    color: Colors.teal[100],
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0),
                      ),
                    ),
                    child: ListView(
                      children: [
                        SizedBox(
                          height: 20.0,
                        ),
                        Row(
                          children: [
                            DashboardOptionButton(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => RegisterComplaint(),
                                  ),
                                );
                              },
                              icon: Icons.app_registration,
                              bName: 'Register Compalint',
                            ),
                            DashboardOptionButton(
                              onTap: () async {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ComplaintStatus(),
                                  ),
                                );
                              },
                              icon: Icons.history,
                              bName: 'Complaint History',
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            DashboardOptionButton(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Profile(),
                                  ),
                                );
                              },
                              icon: Icons.account_box,
                              bName: 'Profile',
                            ),
                            DashboardOptionButton(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EmergencyScreen(),
                                  ),
                                );
                              },
                              icon: Icons.call,
                              bName: 'Emergency',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Alert
  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // back button press alert
  Future<bool> _onBackPressed() async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Do you want Logout from the app ?",
            style: TextStyle(fontFamily: 'PTSans'),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'No',
                style: TextStyle(fontFamily: 'PTSans'),
              ),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
            TextButton(
              child: const Text(
                'Yes',
                style: TextStyle(fontFamily: 'PTSans'),
              ),
              onPressed: () {
                Navigator.pop(context, true);
              },
            ),
          ],
        );
      },
    );
  }

  Future<bool> _onLogout(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Do you want Logout from the app ?",
            style: TextStyle(fontFamily: 'PTSans'),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'No',
                style: TextStyle(fontFamily: 'PTSans'),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: const Text(
                'Yes',
                style: TextStyle(fontFamily: 'PTSans'),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
