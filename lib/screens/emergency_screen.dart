import 'dart:convert';

import 'package:CRS/service/constant.dart';
import 'package:CRS/service/emergency_card.dart';
import 'package:CRS/service/location.dart';
import 'package:CRS/service/user_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class EmergencyScreen extends StatefulWidget {
  @override
  _EmergencyScreenState createState() => _EmergencyScreenState();
}

class _EmergencyScreenState extends State<EmergencyScreen> {
  String distance;
  String address;
  String state;
  String district;
  String division;
  String policeStation;
  String pNo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: kBlue,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: kWhite,
          ),
        ),
        title: Text(
          'Emergency',
          style: TextStyle(
            fontSize: 22,
            color: Colors.white,
            height: 1.5,
            fontFamily: 'PTSans',
          ),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: getFullEmergencyDetails(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return EmergencyDeatils(
                pNo: pNo,
                distance: distance,
                address: address,
                division: division,
                district: district,
                state: state,
                policeStation: policeStation,
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        onPressed: () async {
          final url = "tel:$pNo";
          if (await canLaunch(url)) {
            await launch(url);
          } else {
            throw 'Could not Launch Url';
          }
        },
        child: Icon(
          Icons.call,
          size: 30.0,
          color: Colors.red,
        ),
      ),
    );
  }
  //--------------------------------------------------- Api Call -----

  Future getFullEmergencyDetails() async {
    http.Response response =
        await getEmergency(Location.latitude, Location.longititude);
    print(response.statusCode);
    dynamic data = response.body;
    dynamic decodedData = jsonDecode(data);
    if (response.statusCode == 200 || response.statusCode == 201) {
      pNo = decodedData['phone'];
      policeStation = decodedData['name'];
      double dis = decodedData['distance'];
      address = decodedData['address'];
      state = decodedData['state']['name'];
      district = decodedData['district']['name'];
      division = decodedData['division']['name'];
      if (dis < 1000) {
        var val = dis.round();
        distance = '$val' + ' ' + 'M';
      } else {
        dis = dis / 1000;
        var val = dis.round();
        distance = '$val' + ' ' + 'KM';
      }
    } else {
      _showMyDialog(
          'Error: ${response.statusCode}', '${decodedData['message']}');
    }
  }

  Future<void> _showMyDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class EmergencyDeatils extends StatelessWidget {
  EmergencyDeatils({
    @required this.pNo,
    @required this.distance,
    @required this.address,
    @required this.division,
    @required this.district,
    @required this.state,
    @required this.policeStation,
  });
  final String division;
  final String district;
  final String state;
  final String address;
  final String pNo;
  final String distance;
  final String policeStation;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        children: [
          Center(
            child: Container(
              margin: EdgeInsets.only(top: 20.0),
              height: MediaQuery.of(context).size.height * 0.10,
              child: Image(
                image: AssetImage('images/emergency.png'),
              ),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  policeStation,
                  style: TextStyle(
                    color: Colors.grey.shade700,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'PTSans',
                  ),
                ),
                SizedBox(
                  height: 20.0,
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: Divider(
                    color: Colors.teal[900],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(FontAwesomeIcons.route),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text(
                        distance,
                        style: TextStyle(
                          color: Colors.grey.shade700,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'PTSans',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          Column(
            children: [
              EmergencyCard(
                data: '$pNo',
                icon: Icons.call,
              ),
              SizedBox(
                height: 15.0,
              ),
              EmergencyCard(
                data: '$address',
                icon: FontAwesomeIcons.addressCard,
              ),
              SizedBox(
                height: 15.0,
              ),
              EmergencyCard(
                data: '$division, $district\n $state',
                icon: Icons.add_location,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
