import 'package:CRS/screens/forgot-password.dart';
import 'package:CRS/screens/login-screen.dart';
import 'package:CRS/screens/successful_registration.dart';
import 'package:CRS/service/task_data.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:CRS/screens/first_screen.dart';
import 'package:CRS/screens/create-new-account.dart';
import 'package:provider/provider.dart';

void main() => runApp(TALAASH());

class TALAASH extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TaskListData(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
          splash: Image.asset('images/police.png'),
          nextScreen: HomeScreen(),
          splashTransition: SplashTransition.scaleTransition,
        ),
        routes: {
          'login': (context) => LoginScreen(),
          'home': (context) => HomeScreen(),
          'CreateNewAccount': (context) => CreateNewAccount(),
          'ForgotPassword': (context) => ForgotPassword(),
          'SuccessScreen': (context) => SuccessScreen(),
        },
      ),
    );
  }
}
