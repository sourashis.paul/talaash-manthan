import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  RoundIconButton({this.image, @required this.onTap, this.icon, this.color});
  final String image;
  final Function onTap;
  final IconData icon;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      elevation: 0.0,
      child: image != null
          ? Image.asset(image)
          : Icon(
              icon,
              color: color,
            ),
      onPressed: onTap,
      constraints: BoxConstraints.tightFor(
        width: 150.0,
        height: 120.0,
      ),
      shape: CircleBorder(),
      fillColor: Colors.white,
    );
  }
}
