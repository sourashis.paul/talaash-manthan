import 'package:flutter/material.dart';

class EmergencyCard extends StatelessWidget {
  EmergencyCard({@required this.data, @required this.icon});
  final IconData icon;
  final String data;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      width: MediaQuery.of(context).size.width * 0.90,
      decoration: BoxDecoration(
        border: Border.all(width: 1.0),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ListTile(
        leading: Icon(icon),
        title: Text(
          '$data',
          style: TextStyle(
            color: Colors.grey.shade700,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'PTSans',
          ),
        ),
      ),
    );
  }
}
