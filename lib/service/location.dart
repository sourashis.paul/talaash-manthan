import 'dart:convert';

import 'package:geolocator/geolocator.dart';

class Location {
  static double longititude;
  static double latitude;
  static String userLocation;
  Map<dynamic, dynamic> map;
  void getCurrentLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);
      longititude = position.longitude;
      latitude = position.latitude;
      userLocation = jsonEncode(position);
      print(userLocation);
    } catch (e) {
      print(e);
    }
  }
}
