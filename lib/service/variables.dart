class Variables {
  static String token;
  static String name;
  static String email;
  static String id;
  static String phone;
  static String address;
  static int pinCode;
  static String policeStation;
  static String state;
  static String district;
  static String division;
  static String image;
}
