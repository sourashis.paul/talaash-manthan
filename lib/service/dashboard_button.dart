import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';

class DashboardOptionButton extends StatelessWidget {
  DashboardOptionButton(
      {@required this.onTap, @required this.icon, @required this.bName});
  final Function onTap;
  final IconData icon;
  final String bName;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        child: Container(
          height: 160.0,
          padding: EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 60.0,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                bName,
                style: dashboardButtonTextStyle,
                textAlign: TextAlign.center,
              ),
            ],
          ),
          margin: EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            color: Color(0xFFD6D6D6),
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
