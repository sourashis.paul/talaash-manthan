import 'package:flutter/material.dart';
import 'package:CRS/service/constant.dart';

class BottomNavButton extends StatelessWidget {
  BottomNavButton({@required this.buttonName, @required this.routeName});
  final String buttonName;
  final String routeName;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, routeName);
        },
        child: Container(
          height: 50.0,
          decoration: BoxDecoration(
            color: Color(0xFF12375F),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Center(
            child: Text(
              buttonName,
              style: firstScreenButtonStyle,
            ),
          ),
        ),
      ),
    );
  }
}
