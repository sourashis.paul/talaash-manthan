import 'dart:convert';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:CRS/screens/complaint_details.dart';

class Complaint extends StatefulWidget {
  Complaint({
    @required this.date,
    @required this.id,
    @required this.title,
    @required this.progress,
    @required this.token,
  });

  final String id;
  final String date;

  final String title;
  final String progress;
  final String token;

  @override
  _ComplaintState createState() => _ComplaintState(
        id: id,
        date: date,
        title: title,
        progress: progress,
        token: token,
      );
}

class _ComplaintState extends State<Complaint> {
  _ComplaintState({
    @required this.date,
    @required this.id,
    @required this.progress,
    @required this.title,
    @required this.token,
  });
  final String id;
  final String date;

  final String title;
  final String progress;
  final String token;
  bool showspinner = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ComplaintDetailsScreen(
              cId: id,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Color(0xFFE8E8E8),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _complaintstatus(progress),
                Text(
                  getDate(date),
                  style: TextStyle(fontSize: 14),
                ),
              ],
            ),
            Divider(
              color: Colors.grey,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '#' + id,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: Text(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}

String getDate(String time) {
  var date = DateTime.parse(time);
  String timezone = DateFormat.yMMMd().format(date);
  return timezone;
}

String getTime(String time) {
  var date = DateTime.parse(time);
  String timemode = formatDate(date, [HH, ':', nn, ':', ss]);
  return timemode;
}

Widget _complaintstatus(String status) {
  Icon icon;
  Color color;

  if (status == "PENDING" || status == "PROCESSING" || status == "ON-HOLD") {
    icon = Icon(
      Icons.timer,
      color: Colors.orange,
    );
    color = Colors.orange;
  } else if (status == "RESOLVED" || status == "ACCEPTED") {
    icon = Icon(
      Icons.check,
      color: Colors.green,
    );
    color = Colors.green;
  } else if (status == "REJECTED") {
    icon = Icon(Icons.clear, color: Colors.redAccent);
    color = Colors.redAccent;
  } else {
    icon = Icon(
      Icons.clear,
      color: Colors.redAccent,
    );
    color = Colors.redAccent;
  }

  return Row(
    children: [
      icon,
      Text(
        "$status",
        style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          color: color,
        ),
      ),
    ],
  );
}
