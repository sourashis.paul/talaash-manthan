import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const basicTextStyle = TextStyle(
    fontWeight: FontWeight.bold, fontSize: 28, color: Color(0xFF757575));
const kfirstscreenheadingtextstyle = TextStyle(
  fontSize: 40,
  color: Color(0xFF0D47A1),
  fontWeight: FontWeight.w900,
  fontFamily: 'Poppins',
);
const actorTextStyle = TextStyle(
  fontFamily: 'Poppins',
  color: Color(0xFF0D47A1),
  fontSize: 17,
  fontWeight: FontWeight.w300,
);
const profileheadingTextStyle = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.w500,
  color: Color(0xFF212121),
  fontFamily: 'PTSans',
);
const profileBodyTextStyle = TextStyle(
  fontSize: 17,
  fontFamily: 'PTSans',
  color: Color(0xFF616161),
  fontWeight: FontWeight.w200,
);
const dashboardButtonTextStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
);
const complaintDetailsScreenIdStyle = TextStyle(
  // fontFamily: 'PTSans',
  fontSize: 25.0,
  fontWeight: FontWeight.w900,
);
const complaintDetailsScreenDateStyle = TextStyle(
  color: Color(0xFFFFFFFF),
  fontFamily: 'PTSans',
  fontSize: 15.0,
);
const complaintDetailsScreenTypeStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  fontWeight: FontWeight.bold,
);
const complaintDetailsScreenTitleStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);
const complaintDetailsScreenLocationStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  fontStyle: FontStyle.italic,
);
const complaintDetailsScreenTimeStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  fontStyle: FontStyle.italic,
);
const complaintDetailsScreenStatusStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  color: Color(0xFF023D10),
  fontWeight: FontWeight.bold,
);
const complaintDetailsScreenButtonStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  color: Color(0xFFFFFFFF),
  fontWeight: FontWeight.bold,
);
const firstScreenDescriptionStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 15.0,
  color: Color(0xFF12375F),
  fontWeight: FontWeight.bold,
);
const firstScreenButtonStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 20.0,
  color: Color(0xFFFFFFFF),
  fontWeight: FontWeight.bold,
);
const successScreenTextStyle = TextStyle(
  fontFamily: 'PTSans',
  fontSize: 20.0,
  color: Color(0xFF12375F),
  fontWeight: FontWeight.bold,
);
// From Another Constant.dart file

const TextStyle kBodyText = TextStyle(
    fontSize: 22, color: Colors.black, height: 1.5, fontFamily: 'PTSans');

const Color kWhite = Colors.white;
Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}

final Color kBlue = hexToColor('#12375F');
