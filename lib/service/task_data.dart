import 'dart:convert';

import 'package:CRS/service/my_files.dart';
import 'package:CRS/service/variables.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class TaskListData extends ChangeNotifier {
  List<UplaodFile> files = [];
  void deletetask(int indexat) {
    files.removeAt(indexat);
    // Have to remove from cache also

    notifyListeners();
  }

  void addFiles() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(allowMultiple: true);
    if (result != null) {
      print(result.files.map((e) => e.size).toList());
      print(result.paths);
      files = result.files
          .map(
            (e) => UplaodFile(size: e.size, name: e.name, path: e.path),
          )
          .toList();
      notifyListeners();
    }
  }

  Future<Map> send(
    String title,
    String type,
    String dTime,
    String location,
    String description,
    Map victim,
    Map accused,
    String userLocation,
    String policeStation,
  ) async {
    print(title);
    print(type);
    print(dTime);
    print(location);
    print(description);
    print("Victim: " + jsonEncode(victim));
    print(accused);
    print("UserLocation: " + jsonEncode(userLocation));
    print(policeStation);
    var uri = Uri.parse('https://api-talaash.herokuapp.com/citizen/complaints');
    var request = http.MultipartRequest('POST', uri);
    request.headers['Authorization'] = 'Bearer ' + Variables.token;
    request.fields['title'] = title;
    request.fields['type'] = type;
    request.fields['crimeLocation'] = location;
    request.fields['description'] = description;
    request.fields['timeOfCrime'] = dTime;
    request.fields['victim'] = jsonEncode(victim);
    request.fields['accused'] = jsonEncode(accused);
    request.fields['userLocation'] = userLocation;
    request.fields['policeStation'] = policeStation;
    for (var i = 0; i < files.length; i++) {
      request.files
          .add(await http.MultipartFile.fromPath('files', files[i].path));
    }
    var response = await request.send();
    print(response.statusCode);
    if (response.statusCode == 200 || response.statusCode == 201) {
      files.clear();
    }
    Map gotresponse = Map();
    await gotresponse.addAll({
      'statusCode': response.statusCode,
      'message': await response.stream.bytesToString(),
    });
    return gotresponse;
  }
}
