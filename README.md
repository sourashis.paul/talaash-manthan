# Talaash-Manthan

This repository Contains my Code of Manthan 2021 Project.

## Description
An analysis based project for Identification of the Crime Prone Areas & making the crime investigation process very smooth for
police. For the citizen user we have tried to make a support by making crime registration process online & tracking of crime through this application.

In case of emergency there a emergency button anyone who will press it the co-ordinates(lat&long) will share immediately with the nearest police station & user can call the nearest police station immediately as the app is able to fetch nearest police station details there will be an in-built call button if the user press it the call will automatically go to the nearest police station for help.

I have played the role of Mobile Application Developer. Web-App & Analysis done by teammates. We are
the finalist of Manthan 2021.


## Installation
To Run this project install flutter. To install https://flutter.dev/

<!-- ## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->

<!-- ## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README. -->


<!-- ## Authors and acknowledgment
Show your appreciation to those who have contributed to the project. -->

## Project status
This a prototype of Talaash Mobile App, If you want to contribute do let me know you can mail me sourashispaul@outlook.com with your details.


# UI
![UI of Talaash](images/fullui.jpg?raw=true?raw=true)


